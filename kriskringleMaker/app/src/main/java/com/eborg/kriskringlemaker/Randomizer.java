package com.eborg.kriskringlemaker;

/**
 * Created by Eden on 6/04/2016.
 */

import java.util.ArrayList;
import java.util.Random;

public class Randomizer {

    private ArrayList<String> PeopleList;
    private ArrayList<String> ResultList;
    private Random Rng;
    private ArrayList<String> pList1;
    private ArrayList<String> pList2;

    public Randomizer()
    {
        PeopleList = new ArrayList<String>();
        Rng = new Random();
    }

    public ArrayList<String> getPeopleList()
    {
        return PeopleList;
    }

   public ArrayList<String> getResultList()
    {
        return ResultList;
    }

    public void setResultList(ArrayList<String> NewList)
    {
        ResultList = NewList;
    }

    public boolean ListCompare()
    {
        for (int i = 0; i < PeopleList.size(); i++)
        {
            for (int j = i + 1; j < PeopleList.size(); j++)
            {
                if (PeopleList.get(i) == PeopleList.get(j))
                {
                    return true;
                }
            }
        }

        return false;
    }

    private void SetupLists()
    {
        ResultList = new ArrayList<String>();
        pList1 = new ArrayList<String>();
        pList2 = new ArrayList<String>();

        for (String s: PeopleList)
        {
            pList1.add(s);
            pList2.add(s);
        }
    }

    public void RunRandomizer()
    {
        SetupLists();

        boolean isEmpty = false;

        if (pList1.size() == 0)
            isEmpty = true;

        while (!isEmpty)
        {
            String p1 = pList1.get(0);
            pList1.remove(0);

            int randNum = Rng.nextInt(pList2.size() - 1);

            String p2 = pList2.get(randNum);
            pList2.remove(randNum);

            if (pList1.size() == 0 && p1 == p2)
                SetupLists();
            else if (p1 == p2)
            {
                pList1.add(0, p1);
                pList2.add(p2);
            }
            else
                ResultList.add(p1 + "->" + p2);

            if (pList1.size() == 0)
                isEmpty = true;
            else
                isEmpty = false;
        }
    }
}
