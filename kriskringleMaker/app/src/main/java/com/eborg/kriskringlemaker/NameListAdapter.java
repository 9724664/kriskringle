package com.eborg.kriskringlemaker;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Eden on 8/04/2016.
 */
public class NameListAdapter extends ArrayAdapter<String> {

    private List<String> nameList = new ArrayList<String>();

    static class ItemViewHolder
    {
        TextView name;
    }

    public NameListAdapter (Context context, int resource) {super(context,resource);}

    public void add(String name)
    {
        nameList.add(name);
        super.add(name);
    }

    public int getCount() {return super.getCount();}

    public String getItem(int position){return  this.nameList.get(position);}

    public View getView(int position, View convertView, ViewGroup parent)
    {
        View row = convertView;
        ItemViewHolder viewHolder;

        if(row == null)
        {
            LayoutInflater inflater = (LayoutInflater)this.getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            row = inflater.inflate(R.layout.single_list_item, parent, false);
            viewHolder = new ItemViewHolder();
            viewHolder.name = (TextView)row.findViewById(R.id.listText);
            row.setTag(viewHolder);
        }
        else
        {
            viewHolder = (ItemViewHolder)row.getTag();
        }

        String str = getItem(position);
        viewHolder.name.setText(str);
        return row;
    }
}
